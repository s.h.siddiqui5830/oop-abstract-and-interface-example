<?php
//example
abstract class Public {

    public function Teacher ()
		{
			echo "Hi, I'm John";
		}

    abstract protected function Student ();
}

class John extends Public {

		public function Student () {
			echo "How are you";
		}
	}

    $Mob = new John ();
	$Mob->Student ();
	$Mob->Teacher ();
?>